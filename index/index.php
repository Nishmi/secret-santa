<?php

//~ define('domain', 'http://192.168.2.4');
define('domain', 'https://ss.cryf.in');
define('maxAttempts', 6);
define('admin_email', 'saurabh@mail.cryf.in');
define('records_per_page', 10);
define('max_pagination', 5);
define('amp', '&amp;');

session_start();

// error_reporting(E_ALL);
// ini_set('display_errors', 1);

require '../vendor/autoload.php';

if(isset($_SESSION['_id'])) {
	$loggedIn = true;
}
else {
	$loggedIn = false;
}

Flight::route('GET /', function() {
	global $loggedIn;
	if($loggedIn) {
		require '../pages/home.php';
	}
	else {
		require '../pages/landing.php';
	}
});

Flight::route('GET /explore', function() {
	global $loggedIn;
	if($loggedIn) {
		require '../pages/explore.php';
	}
	else {
		Flight::redirect('/');;
	}
});

Flight::route('GET|POST /login', function() {
	global $loggedIn;
	if($loggedIn) {
		Flight::redirect('/');
	}
	else {
		require '../pages/login.php';
	}
});

Flight::route('GET|POST /register', function() {
	global $loggedIn;
	if($loggedIn) {
		Flight::redirect('/');
	}
	else {
		require '../pages/register.php';
	}
});

Flight::route("GET /verify/@id/@key", function($id, $key) {
	global $loggedIn;
	if($loggedIn) {
		Flight::redirect('/');
	}
	else {
		require '../pages/verify.php';
	}
	
});

Flight::route("GET /join/group/@groupname/@gid", function($groupname, $gid) {
	global $loggedIn;
	if($loggedIn) {
		require '../pages/joingroup.php';
	}
	else {
		Flight::redirect('/');
	}
});

Flight::route("GET /leave/group/@groupname/@gid", function($groupname, $gid) {
	global $loggedIn;
	if($loggedIn) {
		require '../pages/leavegroup.php';
	}
	else {
		Flight::redirect('/');
	}
});

Flight::route("GET /group/@groupname/@gid", function($groupname, $gid) {
	global $loggedIn;
	if($loggedIn) {
		require '../pages/group.php';
	}
	else {
		Flight::redirect('/');
	}
});

Flight::route("GET|POST /join/group/@groupname/@gid/passphrase", function($groupname, $gid) {
	global $loggedIn;
	if($loggedIn) {
		require '../pages/passphrase.php';
	}
	else {
		Flight::redirect('/');
	}
});

Flight::route("GET|POST /create", function() {
	global $loggedIn;
	if($loggedIn) {
		require '../pages/create.php';
	}
	else {
		Flight::redirect('/');
	}
});

Flight::route("GET /groups", function() {
	global $loggedIn;
	if($loggedIn) {
		require '../pages/mygroups.php';
	}
	else {
		Flight::redirect('/');
	}
});

Flight::route("GET /logout", function() {
	global $loggedIn;
	if($loggedIn) {
		require '../pages/logout.php';
	}
	else {
		Flight::redirect('/');
	}
});

Flight::start();
