<?php

if(isset($groupname) and isset($gid)) {
	
	$m = new MongoClient();
	$groups = $m->ss->groups;
	$membership = $m->ss->membership;
	$users = $m->ss->users;
	$notifs = $m->ss->notifications;
	
	$gp = $groups->findOne([
		'_id' => new MongoId($gid)
	]);
	
	if($gp['passphrase'] and
	!$membership->findOne([
		'gid' => new MongoId($gid),
		'uid' => new MongoId($_SESSION['_id'])
	])) {
		Flight::redirect("/join/group/{$groupname}/{$gid}/passphrase");
	}
	
	if($gp) {
		
		$mp = isset($_GET['mp']) ? $_GET['mp'] : 1;
		$np = isset($_GET['np']) ? $_GET['np'] : 1;
		
		if(!(isset($_GET['mset']) or isset($_GET['nset']))) {
			$_GET['nset'] = '';
		}
		elseif(isset($_GET['mset']) and isset($_GET['nset'])) {
			unset($_GET['mset']);
		}

		$members = $membership->find([
			'gid' => new MongoId($gid)
		])->sort([ '$natural' => -1 ])->skip($mp - 1)->limit(records_per_page);
		
		$max_m = ceil($membership->find([
			'gid' => new MongoId($gid)
		])->count() / records_per_page);

		$notifications = $notifs->find([
			'gid' => new MongoId($gid)
		])->sort([ '$natural' => -1 ])->skip($np - 1)->limit(records_per_page);
		
		$max_n = ceil($notifs->find([
			'gid' => new MongoId($gid)
		])->count() / records_per_page);
	}
	
	else {
		Flight::notFound();
	}
	
}

else {
	Flight::notFound();
}

?>

<!DOCTYPE html>
<html>
	<head>
		<title>SS :: <?php echo $groupname; ?></title>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<script src="<?php echo domain; ?>/scripts/bower_components/jquery/dist/jquery.min.js"></script>
		<link rel='stylesheet' href="<?php echo domain; ?>/scripts/bower_components/bootstrap/dist/css/bootstrap.min.css">
		<link rel='stylesheet' href="<?php echo domain; ?>/scripts/bower_components/bootstrap/dist/css/bootstrap-theme.min.css">
		<script src="<?php echo domain; ?>/scripts/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
		<link rel='stylesheet' href="<?php echo domain; ?>/scripts/css/common.css">
		<link rel='stylesheet' href="<?php echo domain; ?>/scripts/css/group.css">
	</head>
	<body>
		<div class='main-panel'>
			<div class='container-fluid'>
				<div class='col-xs-12 col-lg-2'>
					<div class='side-panel'>
						<div class='panel panel-primary'>
							<div class='panel-body'>
								<ul class='nav nav-pills nav-stacked'>
									<li role='presentation'>
										<a href='/'><span class='glyphicon glyphicon-home'></span>&nbsp;&nbsp;Home</a>
									</li>
									<li role='presentation'>
										<a href='/explore'><span class='glyphicon glyphicon-globe'></span>&nbsp;&nbsp;Explore</a>
									</li>
									<li role='presentation'>
										<a href='/groups'><span class='glyphicon glyphicon-th-large'></span>&nbsp;&nbsp;My Groups</a>
									</li>
									<li role='presentation'>
										<a href='/create'><span class=' glyphicon glyphicon-plus-sign'></span>&nbsp;&nbsp;Create</a>
									</li>
								</ul>
							</div>
						</div>
					</div>
				</div>
				<div class='col-lg-8 col-xs-12'>
					<div class='panel panel-primary content'>
						<div class='heading'>
							<h1><?php echo $groupname; ?></h1>
						</div>
						<div class='panel-body'>
							<ul class='nav nav-tabs' role='tablist'>
								<li role='presentation' class='<?php echo (isset($_GET['nset']) ? 'active' : ''); ?>'>
									<a href='#notifications' role='tab' data-toggle='tab'>
										Notifications
										<span class='badge'><?php echo $notifications->count(); ?></span>
									</a>
								</li>
								<li role='presentation' class='<?php echo (isset($_GET['mset']) ? 'active' : ''); ?>'>
									<a href='#members' role='tab' data-toggle='tab'>
										Members
										<span class='badge'><?php echo $members->count(); ?></span>
									</a>
								</li>
							</ul>
							<div class='tab-content'>
								<div role='tabpanel' class='tab-pane <?php echo (isset($_GET['nset']) ? 'active' : ''); ?>' id='notifications'>
									<ul class='list-group'>
										<?php foreach($notifications as $n) { ?>
											<li class='list-group-item'>
												<?php if($n['event'] === 'group.join') { ?>
													<span class='label label-info'>J</span>&nbsp;
													<strong>
														<?php echo $users->findOne([
															'_id' => new MongoId($n['uid'])
														])['name']; ?>
													</strong>
													has <strong>joined</strong> the group
												<?php }
												else { ?>
													<span class='label label-warning'>L</span>&nbsp;
													<strong>
														<?php echo $users->findOne([
															'_id' => new MongoId($n['uid'])
														])['name']; ?>
													</strong>
													has <strong>left</strong> the group
												<?php } ?>
											</li>
										<?php } ?>
									</ul>
									<?php if($max_n > 1) { ?>
									<div class='text-center'>
										<div class='pagination'>
											<li>
												<a href='?<?php echo 'mp='.$mp.amp.'np=1'.amp.'nset='; ?>'>&laquo;</a>
											</li>
											<li>
												<a href='?<?php echo 'np='.($np > 1 ? $np - 1 : 1).amp.'mp='.$mp.amp.'nset='; ?>'>&lsaquo;</a>
											</li>
											<li>
												<a href=''><?php echo $np; ?></a>
											<li>
												<a href='?<?php echo 'np='.($np < $max_n ? $np + 1 : $max_n).amp.'mp='.$mp.amp.'nset='; ?>'>&rsaquo;</a>
											</li>
											<li>
												<a href='?<?php echo 'np='.$max_n.amp.'mp='.$mp.amp.'nset='; ?>'>&raquo;</a>
											</li>
										</div>
									</div>
									<?php } ?>
								</div>
								<div role='tabpanel' class='tab-pane <?php echo (isset($_GET['mset']) ? 'active' : ''); ?>' id='members'>
									<ul class='list-group mem-list'>
										<?php foreach($members as $member) {
											$user = $users->findOne([
												'_id' => new MongoId($member['uid'])
											]);
											$mship = $membership->findOne([
												'gid' => new MongoId($gid),
												'uid' => new MongoId($member['uid'])
											]);
											if($user) { ?>
											<li class='list-group-item'>
												<?php echo $user['name'];
												if($mship['admin'] === true) { ?>
													&nbsp;<span class='label label-danger'>A</span>
												<?php } ?>
											</li>
											<?php }
										} ?>
									</ul>
									<?php if($max_m > 1) { ?>
									<div class='text-center'>
										<div class='pagination'>
											<li>
												<a href='?<?php echo 'np='.$np.amp.'mp=1'.amp.'mset='; ?>'>&laquo;</a>
											</li>
											<li>
												<a href='?<?php echo 'mp='.($mp > 1 ? $mp - 1 : 1).amp.'np='.$np.amp.'mset='; ?>'>&lsaquo;</a>
											</li>
											<li>
												<a href=''><?php echo $mp; ?></a>
											<li>
												<a href='?<?php echo 'mp='.($mp < $max_m ? $mp + 1 : $max_m).amp.'np='.$np.amp.'mset='; ?>'>&rsaquo;</a>
											</li>
											<li>
												<a href='?<?php echo 'mp='.$max_m.amp.'np='.$np.amp.'mset='; ?>'>&raquo;</a>
											</li>
										</div>
									</div>
									<?php } ?>
								</div>
							</div>
							<?php
							$gp = $groups->findOne([
								'_id' => new MongoId($gid)
							]);
							if ($gp) { ?>
							<p><?php echo $gp['description']; ?></p>
							<?php } ?>
							<div class='event'>
								<h3>Event</h3>
								<div class='test'>
									<?php if(isset($_GET['test'])) { ?>
										<div class='test-results'>
											<?php
											require 'algo.php';
											$ran = new Randomizer($gid);
											$rcount = $ran->getCount();
											?>
											<table class='table table-hover'>
												<?php for($i=0;$i<$rcount;$i++) { ?>
												<tr>
													<td class='text-center'>
														<?php
														$pair = $ran->next();
														echo $pair['santa']['name'];
														?>
													</td>
													<td class='text-center'><span class='glyphicon glyphicon-arrow-right'></span></td>
													<td class='text-center'>
														<?php
														echo $pair['friend']['name'];
														?>
													</td>
												</tr>
												<?php } ?>
											</table>
										</div>
									<?php } ?>
									<?php
									if (isset($_SESSION['_id'])) {
										$u = $users->findOne([
											'_id' => new MongoId($_SESSION['_id'])
										]);
										$gp = $groups->findOne([
											'_id' => new MongoId($gid)
										]);
										$startOk = false;
										if (isset($gp['startdate'])) {
											$startOk = date_diff(date_create(), date_create(date('Y-M-d h:i:s', $gp['startdate']->sec)))->d >= 300;
										} else {
											$startOk = true;
										}
										$mems = $membership->findOne([
											'uid' => new MongoId($_SESSION['_id']),
											'gid' => new MongoId($gid)
										]);
										if ($u and $mems and $mems['admin']) {
									?>
									<div class='test-button'>
										<div class='text-center'>
											<a href='?<?php echo 'mp='.$mp.amp.'np='.$np.amp.'start='; ?>' class='<?php if (!$startOk or isset($_GET['start'])) echo 'disabled '; ?>btn btn-primary btn-lg'>
												Start&nbsp;&nbsp;<span class='glyphicon glyphicon-play'></span>
											</a>
										</div>
									</div>
									<?php }
										if (isset($_GET['start']) and $startOk) {
											require 'algo.php';
											require 'startmail.php';
											$ran = new Randomizer($gid);
											for ($i = 0; $i < $ran->getCount(); $i++) {
												$pair = $ran->next();
												$email = $users->findOne([
													'_id' => $pair['santa']['_id']
												])['email'];
												dispatch($pair, $gp, $email);
											}
											$groups->update([
												'_id' => $gp['_id']
											], [
												'$set' => [
													'startdate' => new MongoDate(time())
												]
											]);
									?>
									<p>Mails with the users' randomly selected secret friends have been dispatched!</p>
									<?php	}
									} ?>
									<div class='test-button'>
										<div class='text-center'>
											<a href='?<?php echo 'mp='.$mp.amp.'np='.$np.amp.'test='; ?>' class='btn btn-primary btn-lg'>
												Test&nbsp;&nbsp;<span class='glyphicon glyphicon-flash'></span>
											</a>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class='col-xs-12 col-lg-2'>
					<div class='side-panel2'>
						<div class='panel panel-primary'>
							<div class='panel-body'>
								<ul class='nav nav-pills nav-stacked'>
									<li role='presentation'>
										<?php
										$bool = $membership->findOne([
											'uid' => new MongoId($_SESSION['_id']),
											'gid' => new MongoId($gid)
										]);
										if($bool) { ?>
											<a href='/leave/group/<?php echo htmlspecialchars($groupname, ENT_QUOTES); ?>/<?php echo $gid; ?>'>
											<span class='glyphicon glyphicon-minus-sign'></span>&nbsp;&nbsp;Leave Group
											</a>
										<?php }
										else { ?>
											<a href='/join/group/<?php echo $groupname; ?>/<?php echo $gid; ?>'>
											<span class='glyphicon glyphicon-ok-sign'></span>&nbsp;&nbsp;Join Group
											</a>
										<?php } ?>
									</li>
									<li role='presentation'>
										<a href='/logout'><span class='glyphicon glyphicon-log-out'></span>&nbsp;&nbsp;Logout</a>
									</li>
								</ul>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</body>
</html>
