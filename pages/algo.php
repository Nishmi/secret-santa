<?php

class Randomizer {
	
	private $gid;
	private $count;
	private $mem;
	private $mdarray;
	private $last;
	private $first;
	
	function __construct($g) {
		$this->gid = $g;
		$m = new MongoClient();
		$this->mem = $m->ss->membership;
		$this->count = $this->mem->find([
			'gid' => new MongoId($g)
		])->count();
		$temp = $this->mem->find([
			'gid' => new MongoId($g)
		]);
		$users = $m->ss->users;
		foreach($temp as $t) {
			$te = [];
			$te['_id'] = $t['uid'];
			if($users->findOne([
				'_id' => new MongoId($t['uid'])
			])) {
				$te['name'] = $users->findOne([
					'_id' => new MongoId($t['uid'])
				])['name'];
			}
			else {
				continue;
			}
			$this->mdarray[] = $te;
		}
		shuffle($this->mdarray);
		$this->first = $this->last = array_pop($this->mdarray);
	}
	
	function next() {
		$santa = $this->last;
		$count = sizeof($this->mdarray);
		if($count === 0) {
			$friend = $this->first;
		}
		else {
			$ri = rand(0, $count - 1);
			$ta = [];
			for($i=0;$i<$count;$i++) {
				if($ri === $i) {
					$friend = $this->mdarray[$i];
				}
				else {
					$ta[] = $this->mdarray[$i];
				}
			}
			$this->mdarray = $ta;
			$this->last = $friend;
		}
		if($santa and $friend) {
			return [
				'santa' => $santa,
				'friend' => $friend
			];
		}
		else {
			return false;
		}
	}
	
	function getCount() {
		return $this->count;
	}
	
}
