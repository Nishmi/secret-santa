<?php

$m = new MongoClient();
$notifications = $m->ss->notifications;
$groups = $m->ss->groups;
$users = $m->ss->users;
$mem = $m->ss->membership;

$mems = $mem->find([
	'uid' => new MongoId($_SESSION['_id'])
]);

$data = [];

foreach($mems as $umem) {
	array_push($data, ['gid' => $umem['gid']]);
}

if($data === []) {
	$notifs = false;
}

else {
	$max_n = ceil($notifications->find([
		'$or' => $data
	])->count() / records_per_page);
	
	if(isset($_GET['np'])) {
		$np = $_GET['np'];
	}
	else {
		$np = 1;
	}
	$notifs = $notifications->find([
		'$or' => $data
	])->sort([
		'$natural' => -1
	])->skip(($np - 1) * records_per_page)->limit(records_per_page);
}

?>

<!DOCTYPE html>
<html>
	<head>
		<title>SS :: Home</title>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<script src="<?php echo domain; ?>/scripts/bower_components/jquery/dist/jquery.min.js"></script>
		<link rel='stylesheet' href="<?php echo domain; ?>/scripts/bower_components/bootstrap/dist/css/bootstrap.min.css">
		<link rel='stylesheet' href="<?php echo domain; ?>/scripts/bower_components/bootstrap/dist/css/bootstrap-theme.min.css">
		<link rel='stylesheet' href="<?php echo domain; ?>/scripts/css/common.css">
		<link rel='stylesheet' href="<?php echo domain; ?>/scripts/css/home.css">
	</head>
	<body>
		<div class='main-panel'>
			<div class='container-fluid'>
				<div class='col-xs-12 col-lg-2'>
					<div class='side-panel'>
						<div class='panel panel-primary'>
							<div class='panel-body'>
								<ul class='nav nav-pills nav-stacked'>
									<li role='presentation' class='active'>
										<a href='/'><span class='glyphicon glyphicon-home'></span>&nbsp;&nbsp;Home</a>
									</li>
									<li role='presentation'>
										<a href='/explore'><span class='glyphicon glyphicon-globe'></span>&nbsp;&nbsp;Explore</a>
									</li>
									<li role='presentation'>
										<a href='/groups'><span class='glyphicon glyphicon-th-large'></span>&nbsp;&nbsp;My Groups</a>
									</li>
									<li role='presentation'>
										<a href='/create'><span class=' glyphicon glyphicon-plus-sign'></span>&nbsp;&nbsp;Create</a>
									</li>
								</ul>
							</div>
						</div>
					</div>
				</div>
				<div class='col-lg-8 col-xs-12'>
					<div class='panel panel-primary content'>
						<div class='heading'>
							<h1>Home</h1>
						</div>
						<div class='panel-body'>
							<div class='bg-primary welcome'>
								<h4>
									Welcome
									<em>
										<?php echo $users->findOne([
											'_id' => new MongoId($_SESSION['_id'])
										])['name']; ?>!
									</em>
								</h4>
							</div>
							<h3 class='heading3'>Notifications</h3>
							<?php if($notifs) { ?>
								<div class='notifications'>
									<ul class='list-group'>
										<?php foreach($notifs as $n) { ?>
											<li class='list-group-item'>
												<?php if($n['event'] === 'group.join') { ?>
														<span class='label label-info'>J</span>&nbsp;
														<strong>
															<?php echo $users->findOne([
																'_id' => new MongoId($n['uid'])
															])['name']; ?>
														</strong>
														has <strong>joined</strong> the group
														<strong>
															<?php echo $groups->findOne([
																'_id' => new MongoId($n['gid'])
															])['name']; ?>
														</strong>
													<?php }
													else { ?>
														<span class='label label-warning'>L</span>&nbsp;
														<strong>
															<?php echo $users->findOne([
																'_id' => new MongoId($n['uid'])
															])['name']; ?>
														</strong>
														has <strong>left</strong> the group
														<strong>
															<?php echo $groups->findOne([
																'_id' => new MongoId($n['gid'])
															])['name']; ?>
														</strong>
												<?php } ?>
											</li>
										<?php } ?>
									</ul>
									<?php if($max_n > 1) { ?>
										<div class='text-center'>
											<div class='pagination'>
												<li>
													<a href='<?php echo '?np=1'; ?>'>&laquo;</a>
												</li>
												<li>
													<a href='<?php echo ($np > 1 ? '?np='.($np - 1) : ''); ?>'>&lsaquo;</a>
												</li>
												<li>
													<a><?php echo $np; ?></a>
												</li>
												<li>
													<a href='<?php echo ($np < $max_n ? '?np='.($np + 1) : ''); ?>'>&rsaquo;</a>
												</li>
												<li>
													<a href='<?php echo '?np='.$max_n; ?>'>&raquo;</a>
												</li>
											</div>
										</div>
									<?php } ?>
								</div>
							<?php } ?>
						</div>
					</div>
				</div>
				<div class='col-xs-12 col-lg-2'>
					<div class='side-panel2'>
						<div class='panel panel-primary'>
							<div class='panel-body'>
								<ul class='nav nav-pills nav-stacked'>
									<li role='presentation'>
										<a href='/logout'><span class='glyphicon glyphicon-log-out'></span>&nbsp;&nbsp;Logout</a>
									</li>
								</ul>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</body>
</html>
