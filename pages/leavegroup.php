<?php

if(isset($groupname) and isset($gid) and isset($_SESSION['_id'])) {
	$m = new MongoClient();
	$mem = $m->ss->membership;
	$groups = $m->ss->groups;
	$notifs = $m->ss->notifications;
	
	if(
		$mem->findOne([
			'uid' => new MongoId($_SESSION['_id']),
			'gid' => new MongoId($gid)
		]) and
		$groups->findOne([
			'_id' => new MongoId($gid)
		])
	) {
		
		$isAdmin = $mem->findOne([
			'uid' => new MongoId($_SESSION['_id']),
			'gid' => new MongoId($gid)
		])['admin'];
		
		$mem->remove([
			'uid' => new MongoId($_SESSION['_id']),
			'gid' => new MongoId($gid)
		]);
		
		if($isAdmin) {
			
			$id = $mem->findOne([
				'gid' => new MongoId($gid)
			])['_id'];
			
			$mem->update([
				'_id' => new MongoId($id)
			], [
				'$set' => [
					'admin' => true
				]
			]);
		}
		
		if($mem->find([
			'gid' => new MongoId($gid)
		])->count() === 0) {
			$groups->remove([
				'_id' => new MongoId($gid)
			]);
			$notifs->remove([
				'gid' => new MongoId($gid)
			]);
		}
		
		$noti = [
			'gid' => new MongoId($gid),
			'uid' => new MongoId($_SESSION['_id']),
			'event' => 'group.leave'
		];
		$notifs->insert($noti);
		
		Flight::redirect('/groups');
		
	}
	
	else {
		Flight::notFound();
	}
	
}

else {
	Flight::notFound();
}
