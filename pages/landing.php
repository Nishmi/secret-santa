<!DOCTYPE html>
<html>
	<head>
		<title>SS :: Secret Santa</title>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<script src="<?php echo domain; ?>/scripts/bower_components/jquery/dist/jquery.min.js"></script>
		<link rel='stylesheet' href="<?php echo domain; ?>/scripts/bower_components/bootstrap/dist/css/bootstrap.min.css">
		<link rel='stylesheet' href="<?php echo domain; ?>/scripts/bower_components/bootstrap/dist/css/bootstrap-theme.min.css">
		<link rel='stylesheet' href="<?php echo domain; ?>/scripts/css/common.css">
		<link rel='stylesheet' href="<?php echo domain; ?>/scripts/css/landing.css">
	</head>
	<body>
		<div class='main-panel'>
			<div class='container-fluid'>
				<div class='col-xs-12 col-lg-2'>
					<div class='side-panel'>
						<div class='panel panel-primary'>
							<div class='panel-body'>
								<ul class='nav nav-pills nav-stacked'>
									<li role='presentation' class='active'>
										<a href='/'><span class='glyphicon glyphicon-home'></span>&nbsp;&nbsp;Home</a>
									</li>
									<li role='presentation'>
										<a href='/login'><span class='glyphicon glyphicon-log-in'></span>&nbsp;&nbsp;Login</a>
									</li>
									<li role='presentation'>
										<a href='/register'><span class='glyphicon glyphicon-bookmark'></span>&nbsp;&nbsp;Register</a>
									</li>
								</ul>
							</div>
						</div>
					</div>
				</div>
				<div class='col-lg-8 col-xs-12'>
					<div class='panel panel-primary content'>
						<div class='heading'>
							<h1>The Secret Santa Project <small>V2</small></h1>
						</div>
						<div class='panel-body'>
							<h3>A Warm Welcome</h3>
							<p>
								<strong>Welcome one and all!</strong> This is the third year for the project.
								It had been dormant for about two years now, mostly because I hadn't the
								time to continue it and build it further (due to college and other things).
								I also received complaints that the emails coming from my server were
								going into spam! I finally resolved that, and now the emails are encrypted with
								TLS. The other thing that's changed is, now the whole project has completed an iteration,
								with all its features working normally. So, I'll call it a win for now.
								If you have any queries, suggestions
								or want to communicate with the developer, or bug
								reports, you can find me on <a href="https://discord.gg/UTpDTRU" target="_blank">Discord</a>.
							</p>
							<p>
								It will be great to have you participate in this event! To make things more
								interesting, I have made this an open event where anybody can start their own groups and have this event
								with their group of friends.
							</p>
							<h3>For the new ones</h3>
							<p>
								If you are new here, you might be wondering what this site does. You could
								read more about the
								<a href="https://en.wikipedia.org/wiki/Secret_Santa" target="_blank">tradition</a>.
							</p>
							<p>
								This website lets you and your folks organize yourselves into a group,
								and in these groups the Secret Santa event can be conducted. Each group
								has an admin who can <strong>Start</strong> the event. Starting the event
								runs a randomizing algorithm which assigns Secret Friends to each of the members of that group,
								(worry not, no one will be left behind!) and sends emails to everyone with who they have got
								as their Secret Friend.
							</p>
							<p>
								Once started, the event cannot be started again for almost an year!
								If you would like to test the fairness of the randomizing algorithm,
								there is a button called <strong>Test</strong> created just for that.
							</p>
							<h3>About Groups</h3>
							<p>
								You can create your own group, with any name you like. In fact, to
								allow only the people who you know into the group, you can protect the
								group using a passphrase. On the contrary, groups can also be public! The description
								column tells people what the group is for, or who makes up the group, so
								it would be easier for people to locate it. For example,
								it could be the Graduating class from X Uni of Y batch, etc.
							</p>
							<p>
								If an admin leaves the group a different member in the group gets
								to take their position. So, there is always one admin in every group.
								If all the members leave the group, then the group is removed. However,
								any member can be a part of and create any number of groups.
							</p>
							<p class='made-with-love'>
								Made with <span class='glyphicon glyphicon-heart red'></span> by <a href='https://twitter.com/PhantomBKB'>@PhantomBKB</a>
							</p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</body>
</html>
