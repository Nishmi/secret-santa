<?php

include('../captcha/simple-php-captcha.php');

function checkInput($obj, $min, $max, $regex) {
	if((strlen($obj) < $min) or (strlen($obj) > $max) or !preg_match($regex, $obj)) {
		return false;
	}
	return true;
}

if(isset($_POST['email']) && isset($_POST['password']) && isset($_POST['name']) && isset($_POST['captcha'])) {
	$email = $_POST['email'];
	$password = $_POST['password'];
	$name = $_POST['name'];
	
	$res = checkInput($email, 5, 255, "/^[a-z0-9\Q!#$%&'*+\/=?^_`{}~-\|\E]+(?:\.[a-z0-9\Q!#$%&'*+\/=?^_`{}~-\|\E]+)*\@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?$/");
	
	if($_POST['captcha'] === $_SESSION['captcha']['code']) {
		
		if($res) {
			$m = new MongoClient();
			$users = $m->ss->users;
			
			$doc = $users->findOne([
				'email' => $email
			]);
			
			if(!$doc) {
				$key = uniqid(str_shuffle(md5(str_shuffle($name))), true);
				$data = [
					'name' => htmlspecialchars($name, ENT_QUOTES),
					'email' => $email,
					'password' => password_hash($password, PASSWORD_BCRYPT),
					'verification' => [
						'status' => false,
						'key' => $key
					]
				];
				
				$bool = $users->insert($data);
				
				if($bool) {
					$link = domain."/verify/".$data['_id']."/{$key}";
					require ('../pages/mail.php');
					$alert = 'success';
					$etitle = 'Registered successfully!';
					$emessage = 'An email has been dispatched to your email address. Please verify the same to be able to login.';
				}
				
				else {
					$alert = 'danger';
					$etitle = 'Internal error.';
					$emessage = 'Please report this to the admin.';
				}
			}
			
			else {
				$alert = 'info';
				$etitle = 'Already a member.';
				$emessage = 'Kindly login to proceed.';
			}
		}
		else {
			$alert = 'warning';
			$etitle = 'Invalid email.';
			$emessage = 'Please enter a valid email.';
		}
	}
	else {
		$etitle = 'Invalid captcha.';
		$emessage = '';
		$alert = 'warning';
	}
}

?>

<!DOCTYPE html>
<html>
	<head>
		<title>SS :: Register</title>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<script src="<?php echo domain; ?>/scripts/bower_components/jquery/dist/jquery.min.js"></script>
		<link rel='stylesheet' href="<?php echo domain; ?>/scripts/bower_components/bootstrap/dist/css/bootstrap.min.css">
		<link rel='stylesheet' href="<?php echo domain; ?>/scripts/bower_components/bootstrap/dist/css/bootstrap-theme.min.css">
		<link rel='stylesheet' href="<?php echo domain; ?>/scripts/css/common.css">
		<link rel='stylesheet' href="<?php echo domain; ?>/scripts/css/register.css">
	</head>
	<body>
		<div class='main-panel'>
			<div class='container-fluid'>
				<div class='col-xs-12 col-lg-2'>
					<div class='side-panel'>
						<div class='panel panel-primary'>
							<div class='panel-body'>
								<ul class='nav nav-pills nav-stacked'>
									<li role='presentation'>
										<a href='/'><span class='glyphicon glyphicon-home'></span>&nbsp;&nbsp;Home</a>
									</li>
									<li role='presentation'>
										<a href='/login'><span class='glyphicon glyphicon-log-in'></span>&nbsp;&nbsp;Login</a>
									</li>
									<li role='presentation' class='active'>
										<a href='/register'><span class='glyphicon glyphicon-bookmark'></span>&nbsp;&nbsp;Register</a>
									</li>
								</ul>
							</div>
						</div>
					</div>
				</div>
				<div class='col-lg-4 col-lg-offset-2 col-xs-12'>
					<div class='panel panel-primary content'>
						<div class='heading'>
							<h1>Register</h1>
						</div>
						<div class='panel-body'>
							<?php if(isset($etitle) && isset($emessage) && isset($alert)) { ?>
							<div class='alert alert-<?php echo $alert; ?>'>
								<strong><?php echo $etitle; ?>&nbsp;</strong><?php echo $emessage; ?>
							</div>
							<?php } ?>
							<form method='POST'>
								<div class='form-group'>
									<input class='form-control input-lg' type='text' placeholder='Name' name='name'>
								</div>
								<div class='form-group'>
									<input class='form-control input-lg' type='text' placeholder='Email' name='email'>
								</div>
								<div class='form-group'>
									<input class='form-control input-lg' type='password' placeholder='Password' name='password'>
								</div>
								<div class='form-group'>
									<div class='captcha'>
										<img class='img-responsive center-block' src='<?php $_SESSION['captcha'] = simple_php_captcha();
										echo $_SESSION['captcha']['image_src']; ?>'>
									</div>
									<input type='text' class='form-control' name='captcha' placeholder='Enter the captcha'>
								</div>
								<div class='form-group'>
									<button class='btn btn-default btn-block btn-lg' type='submit'>Register</button>
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
	</body>
</html>

