<?php

if(isset($id) and isset($key)) {

	$m = new MongoClient();
	$users = $m->ss->users;
	
	$doc = $users->findOne([
		'$and' => [
			['_id' => new MongoId(trim($id))],
			['verification.key' => trim($key)]
		]
	]);
	
	if(!$doc) {
		$alert = 'warning';
		$etitle = 'User does not exist or invalid url.';
		$emessage = 'The user specified is not present in our records or the url is invalid.';
	}
	
	else {
			
		$bool = $users->update([
			'email' => $doc['email']
		], [
			'$set' => [
				'verification' => [
					'status' => true
				],
				'attempts' => maxAttempts
			]
		]);
		
		if($bool) {
			$alert = 'success';
			$etitle = 'Verified successfully';
			$emessage = 'You can proceed to login.';
		}
		
		else {
			$alert = 'danger';
			$etitle = 'Internal error.';
			$emessage = 'Please report this to the admin.';
		}
		
	}
			
	
}

else {
	$alert = 'danger';
	$etitle = 'Invalid parameters.';
	$emessage = 'Kindly check the url.';
}

?>

<!DOCTYPE html>
<html>
	<head>
		<title>SS :: Verify</title>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<script src="<?php echo domain; ?>/scripts/bower_components/jquery/dist/jquery.min.js"></script>
		<link rel='stylesheet' href="<?php echo domain; ?>/scripts/bower_components/bootstrap/dist/css/bootstrap.min.css">
		<link rel='stylesheet' href="<?php echo domain; ?>/scripts/bower_components/bootstrap/dist/css/bootstrap-theme.min.css">
		<link rel='stylesheet' href="<?php echo domain; ?>/scripts/css/common.css">
	</head>
	<body>
		<div class='main-panel'>
			<div class='container-fluid'>
				<div class='col-xs-12 col-lg-2'>
					<div class='side-panel'>
						<div class='panel panel-primary'>
							<div class='panel-body'>
								<ul class='nav nav-pills nav-stacked'>
									<li role='presentation'>
										<a href='/'><span class='glyphicon glyphicon-home'></span>&nbsp;&nbsp;Home</a>
									</li>
									<li role='presentation'>
										<a href='/login'><span class='glyphicon glyphicon-log-in'></span>&nbsp;&nbsp;Login</a>
									</li>
									<li role='presentation'>
										<a href='/register'><span class='glyphicon glyphicon-bookmark'></span>&nbsp;&nbsp;Register</a>
									</li>
								</ul>
							</div>
						</div>
					</div>
				</div>
				<div class='col-lg-4 col-lg-offset-2 col-xs-12'>
					<div class='panel panel-primary content'>
						<div class='heading'>
							<h1>Verify</h1>
						</div>
						<div class='panel-body'>
							<?php if(isset($etitle) && isset($emessage) && isset($alert)) { ?>
							<div class='alert alert-<?php echo $alert; ?>'>
								<strong><?php echo $etitle; ?>&nbsp;</strong><?php echo $emessage; ?>
							</div>
							<?php } ?>
						</div>
					</div>
				</div>
			</div>
		</div>
	</body>
</html>
