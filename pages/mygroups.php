<?php

$m = new MongoClient();
$mem = $m->ss->membership;
$groups = $m->ss->groups;

$mres = $mem->find([
	'uid' => new MongoId($_SESSION['_id'])
]);

$count = $mres->count();

$max_pages = ceil($count / records_per_page);

if(isset($_GET['page']) and $_GET['page'] >= 1) {
	$finish = ($count > $_GET['page'] + 2) ? $_GET['page'] + 2 : $max_pages;
	$start = ($_GET['page'] - 2 < 1) ? 1 : $_GET['page'] - 2;
	$finish = ($finish + 4 - ($finish - $start)) > $max_pages ? $max_pages : ($finish + 4 - ($finish - $start));
	$start = ($start - (4 - ($finish - $start))) < 1 ? 1 : ($start - (4 - ($finish - $start)));
	$mres = $mres->skip(($_GET['page'] - 1) * records_per_page)->limit(records_per_page);
	$page = $_GET['page'];
}

else {
	$start = 1;
	$finish = ($max_pages > max_pagination) ? max_pagination : $max_pages;
	$mres = $mres->limit(records_per_page);
	$page = 1;
}

?>

<!DOCTYPE html>
<html>
	<head>
		<title>SS :: My Groups</title>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<script src="<?php echo domain; ?>/scripts/bower_components/jquery/dist/jquery.min.js"></script>
		<link rel='stylesheet' href="<?php echo domain; ?>/scripts/bower_components/bootstrap/dist/css/bootstrap.min.css">
		<link rel='stylesheet' href="<?php echo domain; ?>/scripts/bower_components/bootstrap/dist/css/bootstrap-theme.min.css">
		<script src="<?php echo domain; ?>/scripts/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
		<link rel='stylesheet' href="<?php echo domain; ?>/scripts/css/common.css">
	</head>
	<body>
		<div class='main-panel'>
			<div class='container-fluid'>
				<div class='col-xs-12 col-lg-2'>
					<div class='side-panel'>
						<div class='panel panel-primary'>
							<div class='panel-body'>
								<ul class='nav nav-pills nav-stacked'>
									<li role='presentation'>
										<a href='/'><span class='glyphicon glyphicon-home'></span>&nbsp;&nbsp;Home</a>
									</li>
									<li role='presentation'>
										<a href='/explore'><span class='glyphicon glyphicon-globe'></span>&nbsp;&nbsp;Explore</a>
									</li>
									<li role='presentation' class='active'>
										<a href='/groups'><span class='glyphicon glyphicon-th-large'></span>&nbsp;&nbsp;My Groups</a>
									</li>
									<li role='presentation'>
										<a href='/create'><span class=' glyphicon glyphicon-plus-sign'></span>&nbsp;&nbsp;Create</a>
									</li>
								</ul>
							</div>
						</div>
					</div>
				</div>
				<div class='col-lg-8 col-xs-12'>
					<div class='panel panel-primary content'>
						<div class='heading'>
							<h1>My Groups</h1>
						</div>
						<div class='panel-body'>
							<div class='list-group'>
								<?php foreach($mres as $group) {
									$boo = $groups->findOne([
										'_id' => new MongoId($group['gid'])
									]);
									if($boo) { ?>
									<a class='list-group-item' href='/group/<?php echo $boo['name'].'/'.$boo['_id']; ?>'>
										<div class='list-group-item-heading'>
											<strong><?php echo $boo['name']; ?></strong>&nbsp;
											<?php if($group['admin']) { ?>
												<span class='label label-danger'>A</span>&nbsp;
											<?php }
											else { ?>
												<span class='label label-info'>J</span>&nbsp;
											<?php } ?>
											<span class='label label-primary'>
												<?php echo $mem->find([
													'gid' => new MongoId($boo['_id'])
												])->count(); ?>
											</span>
										</div>
										<div class='list-group-item-text'>
											<?php echo $boo['description']; ?>
										</div>
									</a>
								<?php }
									}
								?>
							</div>
							<?php if(isset($start) and isset($finish) and $max_pages > 1) { ?>
								<div class='text-center'>
									<nav>
										<ul class='pagination'>
											<li>
												<a <?php if($page > 1) { echo 'href=/groups?page='.($page-1); } ?>>&lsaquo;</a>
											</li>
											<?php for($i = $start; $i <= $finish; $i++) { ?>
												<li class='<?php if($i == $page) { echo 'active'; } ?>'><a href='/groups?page=<?php echo $i; ?>'><?php echo $i; ?></a></li>
											<?php } ?>
											<li>
												<a <?php if($page < $max_pages) { echo 'href=/groups?page='.($page+1); } ?>>&rsaquo;</a>
											</li>
										</ul>
									</nav>
								</div>
							<?php } ?>
						</div>
					</div>
				</div>
				<div class='col-xs-12 col-lg-2'>
					<div class='side-panel2'>
						<div class='panel panel-primary'>
							<div class='panel-body'>
								<ul class='nav nav-pills nav-stacked'>
									<li role='presentation'>
										<a href='/logout'><span class='glyphicon glyphicon-log-out'></span>&nbsp;&nbsp;Logout</a>
									</li>
								</ul>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</body>
</html>
