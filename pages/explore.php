<?php

if(isset($_GET['query'])) {
	$query = $_GET['query'];
	
	$m = new MongoClient();
	$groups = $m->ss->groups;
	$membership = $m->ss->membership;
	
	$res1 = $groups->findOne([
		'name' => new MongoRegex("/^{$query}$/")
	]);
	
	if(!isset($_GET['page'])) {

		$res2 = $groups->find([
			'name' => new MongoRegex("/{$query}/i"),
			'_id' => [
				'$ne' => isset($res1) ? $res1['_id'] : null
			]
		])->limit(records_per_page);
		
		$maxPages = ceil($res2->count() / records_per_page);
		
		$start = 1;
		$finish = $maxPages < max_pagination ? $maxPages : max_pagination;
		
		$page = 1;
		
	}
	
	else {
		
		$page = $_GET['page'];
		
		$res2 = $groups->find([
			'name' => new MongoRegex("/{$query}/i"),
			'_id' => [
				'$ne' => isset($res1) ? $res1['_id'] : null
			]
		])->skip(($page - 1) * records_per_page)->limit(records_per_page);
			
		$maxPages = ceil($res2->count() / records_per_page);
		
		if($page > $maxPages or $page < 1) {
			$page = 1;
		}
		
		if($maxPages <= max_pagination) {
			$start = 1;
			$finish = $maxPages;
		}
		
		else {		
			$start = $page - 2 >= 1 ? $page - 2 : 1;
			$finish = ($page - ($page - $start - 2) + 2 > $maxPages) ? $maxPages : $page - ($page - $start - 2) + 2;
			$start = ($finish - $start < max_pagination - 1) ? $page + ($finish - $page - 2) - 2 : $start;
		}
			
	}
}

?>

<!DOCTYPE html>
<html>
	<head>
		<title>SS :: Explore</title>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<script src="<?php echo domain; ?>/scripts/bower_components/jquery/dist/jquery.min.js"></script>
		<link rel='stylesheet' href="<?php echo domain; ?>/scripts/bower_components/bootstrap/dist/css/bootstrap.min.css">
		<link rel='stylesheet' href="<?php echo domain; ?>/scripts/bower_components/bootstrap/dist/css/bootstrap-theme.min.css">
		<link rel='stylesheet' href="<?php echo domain; ?>/scripts/css/common.css">
		<link rel='stylesheet' href="<?php echo domain; ?>/scripts/css/explore.css">
	</head>
	<body>
		<div class='main-panel'>
			<div class='container-fluid'>
				<div class='col-xs-12 col-lg-2'>
					<div class='side-panel'>
						<div class='panel panel-primary'>
							<div class='panel-body'>
								<ul class='nav nav-pills nav-stacked'>
									<li role='presentation'>
										<a href='/'><span class='glyphicon glyphicon-home'></span>&nbsp;&nbsp;Home</a>
									</li>
									<li role='presentation' class='active'>
										<a href='/explore'><span class='glyphicon glyphicon-globe'></span>&nbsp;&nbsp;Explore</a>
									</li>
									<li role='presentation'>
										<a href='/groups'><span class='glyphicon glyphicon-th-large'></span>&nbsp;&nbsp;My Groups</a>
									</li>
									<li role='presentation'>
										<a href='/create'><span class=' glyphicon glyphicon-plus-sign'></span>&nbsp;&nbsp;Create</a>
									</li>
								</ul>
							</div>
						</div>
					</div>
				</div>
				<div class='col-lg-8 col-xs-12'>
					<div class='panel panel-primary content'>
						<div class='heading'>
							<h1>Explore</h1>
						</div>
						<div class='panel-body'>
							<?php if(!isset($query)) { ?>
								<p class='bg-info'>
									<span class='glyphicon glyphicon-info-sign'></span>
									You can search for groups created by people all over the world on this page. Hit <kbd>Enter</kbd>
									to conduct the search. If you want to have a look at all the groups present in our database, search
									with an empty query. Currently, searching by group name is the only search parameter, but
									the first result is a case-sensitive best-match (if one exists). The rest are sorted by age, the
									youngest being the last.
								</p>
							<?php } ?>
							<h3 class='heading3'>Search for a group</h3>
							<form class='search-form form-inline text-center'>
								<div class='form-group'>
									<input type='text' class='form-control input-lg' name='query'>
								</div>
								<button type='submit' class='btn btn-lg btn-default'>Search</button>
							</form>
							<?php if(isset($res2)) { ?>
								
							<div class='search-results'>
								
								<h3>Results</h3>
								
								<ul class='list-group'>
								
									<?php if((!isset($page) or $page == 1) and $res1) { ?>
										
										<a class='list-group-item' href='/group/<?php echo $res1['name']; ?>/<?php echo $res1['_id']; ?>'>
											<div class='list-group-item-heading'>
												<strong><?php echo $res1['name']; ?></strong>&nbsp;
												<?php if($res1['passphrase']) { ?>
													<span class='glyphicon glyphicon-lock'></span>&nbsp;
												<?php } ?>
												<?php $bool = $membership->findOne([
													'$and' => [
														['uid' => new MongoId($_SESSION['_id'])],
														['gid' => new MongoId($res1['_id'])]
													]
												]);
												if($bool) { 
													if($bool['admin']) { ?>
														<div class='label label-danger'>A</div>
													<?php }
													else { ?>
													<div class='label label-info'>J</div>
												<?php }
												} ?>
												<span class='label label-primary'>
													<?php echo $membership->find([
														'gid' => new MongoId($res1['_id'])
													])->count(); ?>
												</span>
											</div>
											<div class='list-group-item-text'>
												<?php echo $res1['description']; ?>
											</div>
										</a>
										
									<?php } ?>
									
									<?php foreach($res2 as $r) { ?>
									
										<a class='list-group-item' href='/group/<?php echo $r['name']; ?>/<?php echo $r['_id']; ?>'>
											<div class='list-group-item-heading'>	
												<strong><?php echo $r['name']; ?></strong>&nbsp;
												<?php if($r['passphrase']) { ?>
													<span class='glyphicon glyphicon-lock'></span>&nbsp;
												<?php } ?>
												<?php $bool = $membership->findOne([
													'$and' => [
														['uid' => new MongoId($_SESSION['_id'])],
														['gid' => new MongoId($r['_id'])]
													]
												]);
												if($bool) { 
													if($bool['admin']) { ?>
														<div class='label label-danger'>A</div>&nbsp;
													<?php }
													else { ?>
													<div class='label label-info'>J</div>&nbsp;
												<?php }
												} ?>
												<span class='label label-primary'>
													<?php echo $membership->find([
														'gid' => new MongoId($r['_id'])
													])->count(); ?>
												</span>
											</div>
											<div class='list-group-item-text'>
												<?php echo $r['description']; ?>
											</div>
										</a>
										
									<?php }	?>
									
								</ul>

							</div>
							
							<?php } ?>
							
							<?php if(isset($start) and isset($finish) and isset($query) and isset($page) and $maxPages > 1) { ?>
							<div class='row'>
								<div class='text-center'>
									<div class='pagination'>
										<li>
											<a <?php if($page > 1) { ?>href='/explore?query=<?php echo $query; ?>&page=<?php echo $page - 1; ?>'<?php } ?>>&lsaquo;</a>
										</li>
										<?php for($i = $start; $i <= $finish; $i++) { ?>
											<li class='<?php if($i == $page) { echo 'active'; } ?>'><a href='/explore?query=<?php echo $query; ?>&page=<?php echo $i; ?>'><?php echo $i; ?></a></li>
										<?php } ?>
										<li>
											<a <?php if($page < $maxPages) { ?>href='/explore?query=<?php echo $query; ?>&page=<?php echo $page + 1; ?>'<?php } ?>>&rsaquo;</a>
										</li>
									</div>
								</div>
							</div>
							<?php } ?>
						</div>
					</div>
				</div>
				<div class='col-xs-12 col-lg-2'>
					<div class='side-panel2'>
						<div class='panel panel-primary'>
							<div class='panel-body'>
								<ul class='nav nav-pills nav-stacked'>
									<li role='presentation'>
										<a href='/logout'><span class='glyphicon glyphicon-log-out'></span>&nbsp;&nbsp;Logout</a>
									</li>
								</ul>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</body>
</html>
