<?php

if(isset($groupname) and isset($gid) and isset($_SESSION['_id'])) {
	//echo $_SESSION['_id'];
	//echo 'hello';
	$m = new MongoClient();
	$groups = $m->ss->groups;
	$membership = $m->ss->membership;
	$notifs = $m->ss->notifications;
	
	$gp = $groups->findOne([
		'_id' => new MongoId($gid)
	]);
	
	$isAlready = $membership->findOne([
		'$and' => [
			['uid' => new MongoId($_SESSION['_id'])],
			['gid' => new MongoId($gid)]
		]
	]);

	if($gp and !$isAlready) {
		
		if($gp['passphrase']) {
			Flight::redirect("/join/group/{$groupname}/{$gid}/passphrase");
		}
		
		$membership->insert([
			'gid' => new MongoId($gid),
			'uid' => new MongoId($_SESSION['_id']),
			'admin' => false
		]);
		
		$notifs->insert([
			'gid' => new MongoId($gid),
			'uid' => new MongoId($_SESSION['_id']),
			'event' => 'group.join'
		]);
		
		Flight::redirect("/group/{$groupname}/{$gid}");
		
	}
	
	else {
		Flight::notFound();
	}
}

else {
	Flight::notFound();
}
