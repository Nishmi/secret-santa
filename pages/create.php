<?php

include('../captcha/simple-php-captcha.php');

if(isset($_POST['groupname']) and isset($_POST['description']) and isset($_SESSION['captcha'])) {
	$m = new MongoClient();
	$groups = $m->ss->groups;
	$membership = $m->ss->membership;
	
	if(!isset($_POST['passphrase'])) {
		$_POST['passphrase'] = '';
	}
	
	$data = [
		'name' => htmlspecialchars($_POST['groupname'], ENT_QUOTES),
		'passphrase' => $_POST['passphrase'] === '' ? null : password_hash($_POST['passphrase'], PASSWORD_BCRYPT),
		'description' => htmlspecialchars($_POST['description'])
	];
	
	if($_POST['captcha'] === $_SESSION['captcha']['code']) {
	
		if($groups->insert($data)) {
			//die((string)$data['_id'].' '.$_SESSION['_id']);	
			$member = [
				'gid' => $data['_id'],
				'uid' => new MongoId($_SESSION['_id']),
				'admin' => true
			];
			if($membership->insert($member)) {
				//die('meow');
				
				$etitle = 'Successfully created!';
				$emessage = 'Click <a class="alert-link" href="/group/'.$data['name'].'/'.(string)$data['_id'].'">here</a> to visit your group page.';
				$alert = 'success';
				
			}
			
			else {
				
				$etitle = 'Could not add you as a member.';
				$emessage = 'This should not happen, but if it does inform us.';
				$alert = 'danger';
				
			}
			
		}
		
		else {
			$etitle = 'Cannot create group.';
			$emessage = 'This is an internal error, kindly report if you see this. Thank you.';
			$alert = 'danger';
		}
		
	}
	
	else {
		$etitle = 'Invalid captcha.';
		$emessage = '';
		$alert = 'warning';
	}
	
}

?>

<!DOCTYPE html>
<html>
	<head>
		<title>SS :: Create</title>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<script src="<?php echo domain; ?>/scripts/bower_components/jquery/dist/jquery.min.js"></script>
		<link rel='stylesheet' href="<?php echo domain; ?>/scripts/bower_components/bootstrap/dist/css/bootstrap.min.css">
		<link rel='stylesheet' href="<?php echo domain; ?>/scripts/bower_components/bootstrap/dist/css/bootstrap-theme.min.css">
		<script src="<?php echo domain; ?>/scripts/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
		<link rel='stylesheet' href="<?php echo domain; ?>/scripts/css/common.css">
		<link rel='stylesheet' href="<?php echo domain; ?>/scripts/css/create.css">
	</head>
	<body>
		<div class='main-panel'>
			<div class='container-fluid'>
				<div class='col-xs-12 col-lg-2'>
					<div class='side-panel'>
						<div class='panel panel-primary'>
							<div class='panel-body'>
								<ul class='nav nav-pills nav-stacked'>
									<li role='presentation'>
										<a href='/'><span class='glyphicon glyphicon-home'></span>&nbsp;&nbsp;Home</a>
									</li>
									<li role='presentation'>
										<a href='/explore'><span class='glyphicon glyphicon-globe'></span>&nbsp;&nbsp;Explore</a>
									</li>
									<li role='presentation'>
										<a href='/groups'><span class='glyphicon glyphicon-th-large'></span>&nbsp;&nbsp;My Groups</a>
									</li>
									<li role='presentation' class='active'>
										<a href='/create'><span class=' glyphicon glyphicon-plus-sign'></span>&nbsp;&nbsp;Create</a>
									</li>
								</ul>
							</div>
						</div>
					</div>
				</div>
				<div class='col-lg-8 col-xs-12'>
					<div class='panel panel-primary content'>
						<div class='heading'>
							<h1>Create</h1>
						</div>
						<div class='panel-body'>
								
							<?php if(isset($etitle) and isset($emessage) and isset($alert)) { ?>
							<div class='alert alert-<?php echo $alert; ?>'>
								<strong><?php echo $etitle; ?></strong>&nbsp;<?php echo $emessage; ?>
							</div>
							<?php }
							else { ?>
								<p class='bg-info'>
									<span class='glyphicon glyphicon-info-sign'></span>
									You can create your own group using this form. Enter a passphrase if you want only select
									members to be a part of your group. Keeping a passphrase for your group will also maintain your
									privacy by allowing only people who know the passphrase to view the members and other internal
									details of your group.
								</p>
							<?php } ?>
							<form method='post'>
								<div class='form-group'>
									<label>Group name</label>
									<input type='text' class='form-control' name='groupname'>
								</div>
								<div class='form-group'>
									<label>Passphrase (optional)</label>
									<input type='text' class='form-control' name='passphrase'>
								</div>
								<div class='form-group'>
									<label>Description</label>
									<textarea class='form-control' rows='3' name='description'></textarea>
								</div>
								<div class='form-group'>
									<div class='container-fluid'>
										<div class='col-lg-4 col-lg-offset-4'>
											<div class='captcha'>
												<img class='img-responsive center-block' src='<?php $_SESSION['captcha'] = simple_php_captcha();
												echo $_SESSION['captcha']['image_src']; ?>'>
											</div>
											<input type='text' class='form-control' name='captcha' placeholder='Enter the captcha'>
										</div>
									</div>
								</div>
								<div class='form-group'>
									<input type='submit' class='btn btn-default' value='Create Group'>
								</div>
							</form>
						</div>
					</div>
				</div>
				<div class='col-xs-12 col-lg-2'>
					<div class='side-panel2'>
						<div class='panel panel-primary'>
							<div class='panel-body'>
								<ul class='nav nav-pills nav-stacked'>
									<li role='presentation'>
										<a href='/logout'><span class='glyphicon glyphicon-log-out'></span>&nbsp;&nbsp;Logout</a>
									</li>
								</ul>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</body>
</html>
