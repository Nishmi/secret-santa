<?php

use PHPMailer\PHPMailer\PHPMailer;

require('../pages/mail.config.php');

// $m = new MongoClient();
// $users = $m->ss->users;

function dispatch($pair, $gp, $email) {
  $config = getConfig();
  
  $mail = new PHPMailer();
  #$mail->SMTPDebug = 2;
  $mail->isSMTP();
  $mail->Host = 'mail.cryf.in';
  $mail->SMTPAuth = true;
  $mail->Username = $config['username'];
  $mail->Password = $config['password'];
  $mail->SMTPSecure = 'tls';

  $santa = $pair['santa']['name'];
  $gpname = $gp['name'];
  $friend = $pair['friend']['name'];

  $message = <<<HERE

Hello $santa,
  
  Your Secret Friend from $gpname is
  
  $friend
  
Merry Christmas!

HERE;

  $mail->Subject = "Your Secret Friend";
  $mail->Body = $message;
  $mail->setFrom(admin_email, 'Saurabh');
  $mail->addAddress($email);
  $mail->addReplyTo(admin_email, 'Saurabh');

  $mail->send();
}