<?php

$m = new MongoClient();
$mem = $m->ss->membership;
$groups = $m->ss->groups;
$notifs = $m->ss->notifications;

$gp = $groups->findOne([
	'_id' => new MongoId($gid)
]);

if($gp) {

	$one = $mem->findOne([
		'uid' => new MongoId($_SESSION['_id']),
		'gid' => new MongoId($gid)
	]);

	if($one) {
		Flight::redirect("/group/{$groupname}/{$gid}");
	}

	if(!$gp['passphrase']) {
		Flight::redirect("/group/{$groupname}/{$gid}");
	}

	if(isset($_POST['p'])) {
		
		if(password_verify($_POST['p'], $gp['passphrase'])) {
			//die('hello');	
			$memba = [
				'uid' => new MongoId($_SESSION['_id']),
				'gid' => new MongoId($gid),
				'admin' => false
			];
			$mem->insert($memba);
			
			$noti = [
				'gid' => new MongoId($gid),
				'uid' => new MongoId($_SESSION['_id']),
				'event' => 'group.join'
			];
			$notifs->insert($noti);
			
			Flight::redirect("/group/{$groupname}/{$gid}");
			
		}
		else {
			$etitle = 'Invalid passphrase.';
			$emessage = '';
			$alert = 'danger';
		}
	}
}

?>

<!DOCTYPE html>
<html>
	<head>
		<title>SS :: <?php echo $groupname; ?></title>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<script src="<?php echo domain; ?>/scripts/bower_components/jquery/dist/jquery.min.js"></script>
		<link rel='stylesheet' href="<?php echo domain; ?>/scripts/bower_components/bootstrap/dist/css/bootstrap.min.css">
		<link rel='stylesheet' href="<?php echo domain; ?>/scripts/bower_components/bootstrap/dist/css/bootstrap-theme.min.css">
		<script src="<?php echo domain; ?>/scripts/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
		<link rel='stylesheet' href="<?php echo domain; ?>/scripts/css/common.css">
	</head>
	<body>
		<div class='main-panel'>
			<div class='container-fluid'>
				<div class='col-xs-12 col-lg-2'>
					<div class='side-panel'>
						<div class='panel panel-primary'>
							<div class='panel-body'>
								<ul class='nav nav-pills nav-stacked'>
									<li role='presentation'>
										<a href='/'><span class='glyphicon glyphicon-home'></span>&nbsp;&nbsp;Home</a>
									</li>
									<li role='presentation'>
										<a href='/explore'><span class='glyphicon glyphicon-globe'></span>&nbsp;&nbsp;Explore</a>
									</li>
									<li role='presentation'>
										<a href='/groups'><span class='glyphicon glyphicon-th-large'></span>&nbsp;&nbsp;My Groups</a>
									</li>
									<li role='presentation'>
										<a href='/create'><span class=' glyphicon glyphicon-plus-sign'></span>&nbsp;&nbsp;Create</a>
									</li>
								</ul>
							</div>
						</div>
					</div>
				</div>
				<div class='col-lg-4 col-lg-offset-2 col-xs-12'>
					<div class='panel panel-primary content'>
						<div class='heading'>
							<h1><?php echo $groupname; ?></h1>
						</div>
						<div class='panel-body'>
							<?php if(isset($etitle) and isset($emessage) and isset($alert)) { ?>
								<div class='alert alert-<?php echo $alert; ?>'>
									<strong><?php echo $etitle; ?></strong>
									<?php echo $emessage; ?>
								</div>
							<?php } ?>
							<form method='post'>
								<div class='form-group'>
									<label>Enter the passphrase</label>
									<input type='password' class='form-control input-lg' name='p'>
								</div>
								<div class='form-group'>
									<input type='submit' class='btn btn-default btn-block btn-lg' value='Join Group'>
								</div>
							</form>
						</div>
					</div>
				</div>
				<div class='col-xs-12 col-lg-2 col-lg-offset-2'>
					<div class='side-panel2'>
						<div class='panel panel-primary'>
							<div class='panel-body'>
								<ul class='nav nav-pills nav-stacked'>
									<li role='presentation'>
										<a href='/logout'><span class='glyphicon glyphicon-log-out'></span>&nbsp;&nbsp;Logout</a>
									</li>
								</ul>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</body>
</html>
