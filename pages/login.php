<?php

if(isset($_POST['email']) && isset($_POST['password'])) {
	$email = $_POST['email'];
	$password = $_POST['password'];
	
	$m = new MongoClient();
	$users = $m->ss->users;
	
	$doc = $users->findOne([
		'email' => $email
	]);
	
	if($doc){
		if($doc['verification']['status']) {
			if($doc['attempts'] > 0) {
				if(password_verify($password, $doc['password'])) {
					$_SESSION['_id'] = (string)$doc['_id'];
					$users->update([
						'email' => $email
					], [
						'$set' => [
							'attempts' => maxAttempts
						]
					]);
					Flight::redirect('/');
				}
				
				else {
					$attemptsRemaining = $doc['attempts'] - 1;
					$users->update([
						'email' => $email
					], [
						'$inc' => [
							'attempts' => -1
						]
					]);
					$etitle = 'Invalid password.';
					$emessage = '';
					$alert = 'danger';
				}
			}
			else {
				$etitle = 'Account blocked.';
				$emessage = 'You have run out of login attempts! Contact the admin for help.';
				$alert = 'danger';
			}
		}
		else {
			$etitle = 'Verify your email.';
			$emessage = 'Kindly verify your email before you log in.';
			$alert = 'info';
		}
	}
	else {
		$etitle = 'User does not exist.';
		$emessage = 'That email id is not in our records.';
		$alert = 'warning';
	}
}

?>
<!DOCTYPE html>
<html>
	<head>
		<title>SS :: Login</title>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<script src="<?php echo domain; ?>/scripts/bower_components/jquery/dist/jquery.min.js"></script>
		<link rel='stylesheet' href="<?php echo domain; ?>/scripts/bower_components/bootstrap/dist/css/bootstrap.min.css">
		<link rel='stylesheet' href="<?php echo domain; ?>/scripts/bower_components/bootstrap/dist/css/bootstrap-theme.min.css">
		<link rel='stylesheet' href="<?php echo domain; ?>/scripts/css/common.css">
	</head>
	<body>
		<div class='main-panel'>
			<div class='container-fluid'>
				<div class='col-xs-12 col-lg-2'>
					<div class='side-panel'>
						<div class='panel panel-primary'>
							<div class='panel-body'>
								<ul class='nav nav-pills nav-stacked'>
									<li role='presentation'>
										<a href='/'><span class='glyphicon glyphicon-home'></span>&nbsp;&nbsp;Home</a>
									</li>
									<li role='presentation' class='active'>
										<a href='/login'><span class='glyphicon glyphicon-log-in'></span>&nbsp;&nbsp;Login</a>
									</li>
									<li role='presentation'>
										<a href='/register'><span class='glyphicon glyphicon-bookmark'></span>&nbsp;&nbsp;Register</a>
									</li>
								</ul>
							</div>
						</div>
					</div>
				</div>
				<div class='col-lg-4 col-lg-offset-2 col-xs-12'>
					<div class='panel panel-primary content'>
						<div class='heading'>
							<h1>Login</h1>
						</div>
						<div class='panel-body'>
							<?php if(isset($alert) and isset($emessage) and isset($etitle)) { ?>
								<div class='alert alert-<?php echo $alert; ?>'>
									<strong><?php echo $etitle; ?></strong>
									<?php echo $emessage; ?>
								</div>
							<?php } ?>
							<?php if(isset($attemptsRemaining)) { ?>
								<div class='attempts'>
									<label>Login attempts remaining:
										<strong class='text-danger'>&nbsp;<?php echo $attemptsRemaining; ?></strong>
									</label>
								</div>
							<?php } ?>
							<form method='POST'>
								<div class='form-group'>
									<input class='form-control input-lg' type='text' placeholder='Email' name='email'>
								</div>
								<div class='form-group'>
									<input class='form-control input-lg' type='password' placeholder='Password' name='password'>
								</div>
								<div class='form-group'>
									<button class='btn btn-default btn-block btn-lg' type='submit'>Login</button>
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
	</body>
</html>
