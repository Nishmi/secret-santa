<?php

use PHPMailer\PHPMailer\PHPMailer;

require('../pages/mail.config.php');

$config = getConfig();

$mail = new PHPMailer();
#$mail->SMTPDebug = 2;
$mail->isSMTP();
$mail->Host = 'mail.cryf.in';
$mail->SMTPAuth = true;
$mail->Username = $config['username'];
$mail->Password = $config['password'];
$mail->SMTPSecure = 'tls';


$message = <<<HERE

Hello $name,
	
	Welcome to The Secret Santa Project! Thank you for registering with us. Kindly verify your email
	by clicking on the link below.
	
	$link
	
Regards,
Saurabh

HERE;

$mail->Subject = "Please verify your email";
$mail->Body = $message;
$mail->setFrom(admin_email, 'Saurabh');
$mail->addAddress($email);
$mail->addReplyTo(admin_email, 'Saurabh');

$mail->send();

?>
